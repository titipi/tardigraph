import flask
import subprocess
import graphviz
import json
from urllib.request import urlopen
from urllib.parse import urlencode
from settings import *
from werkzeug.middleware.proxy_fix import ProxyFix
from flask_basicauth import BasicAuth
from flask_httpauth import HTTPBasicAuth
import os
import subprocess

# Create the application.
app = flask.Flask(__name__, static_url_path="/", static_folder=STATIC_URL)
app.wsgi_app = ProxyFix(app.wsgi_app, x_prefix=1)
app.config['BASIC_AUTH_USERNAME'] = 'sonda'
app.config['BASIC_AUTH_PASSWORD'] = 'samebutnotsame'

basic_auth = BasicAuth(app)

auth = HTTPBasicAuth()
USER_DATA = {
    "altas": "latencias",
    "sonda": "samebutnotsame",
}

@auth.verify_password
def verify(username, password):
    if not (username and password):
        return False
    return USER_DATA.get(username) == password


### FUNCTIONS

def get_pad_content(pad_name):

    print(pad_name)

    arguments = {
        'padID' : pad_name,
        'apikey' : PAD_API_KEY
    }
    api_call = 'getText'
    response = json.load(urlopen(PAD_API_URL+api_call, data=urlencode(arguments).encode()))

    # create pad in case it does not yet exist
    if response['code'] == 1 and 'padID does not exist' == response['message']:
        return False
#        api_call = 'createPad'
#        urlopen(PAD_API_URL+api_call, data=urlencode(arguments).encode())
#        api_call = 'getText'
#        response = json.load(urlopen(PAD_API_URL+api_call, data=urlencode(arguments).encode()))

    content = response['data']['text']
    return content

def all_pads():
    arguments = {
        'apikey' : PAD_API_KEY,
    }
    api_call = 'listAllPads'
    response = json.load(urlopen(PAD_API_URL+api_call, data=urlencode(arguments).encode()))

    return response

def deletepad(pagename):
    pad_name=pagename
    arguments = {
        'padID' : pad_name,
        'apikey' : PAD_API_KEY
    }
    api_call = 'deletePad'
    response = json.load(urlopen(PAD_API_URL+api_call, data=urlencode(arguments).encode()))

    return response

def check_update(filename,content):
    filepath=STATIC_FOLDER+'pad/'+filename
    if os.path.isfile(filepath):
        with open(filepath,'r') as f:
            data = f.read()
        if data == content:
            return False,0,0
    content,inclines,firstindex=transclude(content)
    process_comment(filename,content)
    with open(filepath,'w') as f:
        f.write(content)

    return content,inclines,firstindex

def process_comment(filename,content):
    filepath=STATIC_FOLDER+'comments/'+filename
    contstr=''.join(content)       
    if 'comment="' in contstr:
            comment=contstr.split('comment="')[1].split('"')[0]
            with open(filepath,'w') as f:
                f.write(comment)


def get_engine(firstline,secondline):
    if 'PUBLISH' in firstline:
        firstline=secondline
    if '#' in firstline:
        if 'sfdp' in firstline:
            engine='sfdp'
        elif 'fdp' in firstline:
            engine='fdp'
        elif 'twopi' in firstline:
            engine='twopi'
        elif 'neato' in firstline:
            engine='neato'
        elif 'circo' in firstline:
            engine='circo'
        elif 'fdp' in firstline:
            engine='fdp'
        elif 'nop2' in firstline:
            engine='nop2'
        elif 'nop' in firstline:
            engine='nop'
        elif 'osage' in firstline:
            engine='osage'
        elif 'patchwork' in firstline:
            engine='patchwork'
        else:
            engine='dot'
    else:
        engine='dot'
    return engine

def publish(firstline,secondline,filename):
    filepath=STATIC_FOLDER+'pub/'+filename
    if 'PUBLISH' in firstline or 'PUBLISH' in secondline:
        fil=open(filepath,'w')
        fil.close()
    else:
        if os.path.exists(filepath):
            os.remove(filepath)

def publics():
    filepath=STATIC_FOLDER+'pub/'
    pubs=os.listdir(filepath)
    return pubs

def lgpdfs():
    filepath=STATIC_FOLDER+'large-pdf/'
    lgpdfs=[k.rsplit('-',1)[0] for k in os.listdir(filepath)]
    return lgpdfs

def a3pdfs():
    filepath=STATIC_FOLDER+'tiled-pdf/'
    a3pdfs=[k.rsplit('-',1)[0] for k in os.listdir(filepath)]
    return a3pdfs

def list_comments():
    filepath=STATIC_FOLDER+'comments/'
    comments={}
    for k in os.listdir(filepath):
        with open( filepath+k, 'r') as f:
            kc=f.read()
            kk=k.split('-',1)[0]
            comments[kk]=kc
    return comments

def transclude(source):
    sourcelines=source.split('\n')
    includelines=0
    include=False
    firstindex=False
    for index,line in enumerate(sourcelines):
        if '{{' in line and '}}' in line:
            include=line.split('{{')[1].split('}}')[0]
            pad_content=get_pad_content(include+'-include')
            if pad_content:
                source=source.replace(line,pad_content)
                includelines+=len(pad_content.split('\n'))-1
                firstindex=firstindex or index
    return source,includelines,firstindex


def generate_svg(filename,inclines=0,firstindex=0):
    if '-asn' not in filename:
        sourcepath=STATIC_FOLDER+'pad/'+filename+'-graph'
    else:
        sourcepath=STATIC_FOLDER+'pad/'+filename
    svgpath=STATIC_FOLDER+'svg/'+filename+'.svg'
    newsvgpath=STATIC_FOLDER+'svg/'+filename+'-new.svg'
    pngpath=STATIC_FOLDER+'png/'+filename+'.png'
    pdfpath=STATIC_FOLDER+'pdf/'+filename+'.pdf'
    logpath=STATIC_FOLDER+'log/'+filename+'-log.txt'
    with open(sourcepath,'r') as f:
        source = f.read()
        f.seek(0)
        firstline = f.readline()
        f.seek(1)
        secondline = f.readline()
    dot=graphviz.Source(source)
    engine=get_engine(firstline,secondline)
    public=publish(firstline,secondline,filename)
    try:
        dot.render(format='svg',engine=engine,outfile=newsvgpath)
        dot.render(format='png',engine=engine,outfile=pngpath)
        dot.render(format='pdf',engine=engine,outfile=pdfpath)
        addstyle(newsvgpath,svgpath)
        # os.rename(newsvgpath,svgpath)
        with open(logpath,'w') as fl:
            fl.write('')
            fl.close()
        return True
    except subprocess.CalledProcessError as err:
        print(err)
        insidequotes = False
        extralines=0
        potextralines=0
        for char in source:
            if char=='"':
                insidequotes = not insidequotes
                extralines+=potextralines
                potextralines=0
            elif char=='\n' and insidequotes:
                potextralines+=1
        stderror = err.stderr.decode()
        if 'in line ' in stderror:
           prelines=stderror.split('in line ',1)[0]+'in line '
           oldlines=int(stderror.split('in line ',1)[1].split(' ',1)[0])
           postlines=' '+stderror.split('in line ',1)[1].split(' ',1)[1]
           newlines=str(oldlines+extralines-inclines)
           print(oldlines,firstindex,inclines)
           if oldlines > firstindex and oldlines <= firstindex+inclines:
               stderror=prelines+'in the {{include}} '+postlines
           else:
               stderror=prelines+newlines+postlines
        with open(logpath,'w') as fl:
            fl.write(stderror)
            fl.close()
        return False
    except FileNotFoundError as err:
        print(err)
        return False

def generate_pdfs(pagename,scale):
    origpdf=(STATIC_FOLDER+'pdf/'+pagename+'.pdf')
    if os.path.isfile(origpdf):
        destlg=STATIC_FOLDER+'large-pdf/'+pagename+'-large.pdf'
        desta3=STATIC_FOLDER+'tiled-pdf/'+pagename+'-a3.pdf'
        size=scale+" "+scale
        makelg=subprocess.run(['cpdf','-scale-page',size,origpdf,'-o',destlg],shell=False)
        makea3=subprocess.run(['pdfposter','-m','a3','-s',scale,origpdf,desta3],shell=False)
    else:
        return False



def addstyle(newpath,oldpath):
    with open(newpath, 'r') as newfile, open(oldpath, 'w') as oldfile:
        once=False
        for line in newfile:
            oldfile.write(line)
            if not once and 'www.w3.org/2000/svg' in line:
                oldfile.write("<style>")
                oldfile.write("@import url('http://sonda.para-doxa.org/css/fonts.css");
                oldfile.write('</style>')
                once=True


### ROUTES

@app.route("/", methods=['GET'])
def hello_world():
    return flask.render_template('./flask/index.html', content="Hello World")

@app.route("/listall", methods=['GET'])
@auth.login_required
def hello_listall():
    pads=all_pads()['data']['padIDs']
    graphs=[k.replace('-graph','') for k in pads if k.endswith('-graph')]
    includes=[k.replace('-include','') for k in pads if k.endswith('-include')]
    large_pdfs=lgpdfs()
    tiled_pdfs=a3pdfs()
    comments=list_comments()
    return flask.render_template('./flask/listall.html', comments=comments, graphs=graphs,large_pdfs=large_pdfs,tiled_pdfs=tiled_pdfs,includes=includes)

@app.route("/list", methods=['GET'])
def hello_list():
    graphs=publics()
    large_pdfs=lgpdfs()
    tiled_pdfs=a3pdfs()
    comments=list_comments()
    return flask.render_template('./flask/list.html', comments=comments, graphs=graphs,large_pdfs=large_pdfs,tiled_pdfs=tiled_pdfs)

@app.route("/listasn", methods=['GET'])
@auth.login_required
def hello_listasn():
    pads=all_pads()['data']['padIDs']
    graphs=[k.replace('-asn','') for k in pads if  k.endswith('-asn')]
    includes=[]
    return flask.render_template('./flask/listasn.html', graphs=graphs,includes=includes)

@app.route('/graph/<pagename>', methods=['GET'])
def graph(pagename):
	return flask.render_template('./flask/graph.html', graph=pagename )

@app.route('/g/<pagename>', methods=['GET'])
def ggg(pagename):
	return flask.render_template('./flask/g.html', graph=pagename )

@app.route('/asn/<pagename>', methods=['GET'])
def graphasn(pagename):
	return flask.render_template('./flask/graphasn.html', graph=pagename )

@app.route('/delete/<pagename>', methods=['GET'])
@auth.login_required
def delete(pagename):
    result=deletepad(pagename)
    if result['code']==0:
        return '<html><head><meta http-equiv="refresh" content="3; url=/listall" /></head><body> graph '+pagename+' deleted.</body></html>'

@app.route('/pdfgen/<pagename>/<scale>', methods=['GET'])
def pdfgen(pagename,scale):
    result=generate_pdfs(pagename,scale)
    return '<html><head><meta http-equiv="refresh" content="5; url=/listall" /></head><body> generating scaled up x'+scale+' print pdfs for <b>'+pagename+'</b>... it can take a few seconds.</body></html>'

@app.route('/svg/<pagename>.svg', methods=['GET'])
def svg(pagename):
    if '-asn' not in pagename:
        pad_name=pagename+'-graph'
    else:
        pad_name=pagename
    txt_content=get_pad_content(pad_name)
    if txt_content:
        svgpath=STATIC_FOLDER+'svg/'+pagename+'.svg'
        check,inclines,firstindex=check_update(pad_name,txt_content)
        exist=os.path.isfile(svgpath)
        generated=False
        if check or not exist:
            generated=generate_svg(pagename,inclines,firstindex)
        if generated or exist:
            return flask.send_file(svgpath)
    return 'file not generated yet, you can start by adding this to the pad: <br> digraph { <br> si <br> }'

@app.route('/txt/<pagename>.txt', methods=['GET'])
def txt(pagename): 
    if '-asn' not in pagename:
        pad_name=pagename+'-graph'
    else:
        pad_name=pagename
    txt_content=get_pad_content(pad_name)
    check,inclines,firstindex=check_update(pad_name,txt_content)
    txt_content=check or txt_content
    return txt_content

if __name__ == '__main__':
	app.debug=True
	app.run(port=f'{ PORTNUMBER }')

