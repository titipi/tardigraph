#!/bin/bash

REFRESH_RATE=10

if [ -z $1 ]; then
echo "Tardigraph, a script for collective graphviz diagramming"
echo ""
echo "Enter the link to an online or local etherpad sheet, such as for example:"
echo "http://localhost:9001/p/live-entanglements"
echo "or"
echo "https://pad.constantvzw.org/p/live-entanglements"
echo ""
echo -n "> "
read _PADLINK_
else
_PADLINK_=$1
fi

_DIAG_=${_PADLINK_##*/}

sed "s@_PADLINK_@${_PADLINK_}@g" template.html > $_DIAG_.html
sed -i "s@_DIAG_@${_DIAG_}@g" $_DIAG_.html

firefox $_DIAG_.html

if [ ! -s $_DIAG_.svg ]; then
cat <<EOT >> $_DIAG_.svg
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN"
 "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
<svg width="1000" height="600" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<text text-anchor="left" x="100" y="100" font-size="30.00">to start with the diagram, delete the welcome text</text>
<text text-anchor="left" x="100" y="200" font-size="30.00">digraph {</text>
<text text-anchor="left" x="100" y="300" font-size="30.00">}</text>
</svg>
EOT

fi

while true
do
sleep $REFRESH_RATE
wget -O $_DIAG_.txt $_PADLINK_/export/txt
cat $_DIAG_.txt | sfdp -Tsvg -o $_DIAG_-new.svg &> $_DIAG_-log.txt
cat $_DIAG_.txt | sfdp -Tpdf -o $_DIAG_.pdf
if [ -s $_DIAG_-new.svg ]; then
	mv $_DIAG_-new.svg $_DIAG_.svg
fi

#uncomment to produce printable pdfs with specific stylesheets...
#rsvg-convert -u -w 9933 -f svg -o _DIAG_-lg.svg _DIAG_.svg
#cp _DIAG_.pdf _DIAG_-old.pdf
#convert _DIAG_-lg.svg -density 300 _DIAG_.pdf 

done
