# tardigraph

## what is

A script to generate live editable [Graphviz](https://graphviz.org/) diagrams. It has been written after a few situations of collectively drawing a Graphviz diagram, motivated by the fascination of how diagramming both influences and is in friction with our understanding of relations. It brings together the moment of considering with others how something "is the same as" "is NOT" or maybe "is sort of like" something else, next to the limited mode in which these relations are translated into [the network imaginaries of AT&T](https://www.ocf.berkeley.edu/~eek/index.html/tiny_examples/thinktank/src/gv1.7c/doc/dotguide.pdf).

To allow live editing, this script requires an Etherpad installation running somewhere, either locally or on a friendly instance, as long as the web server allows the etherpad page to be loaded inside an iframe. ( Some work with [CORS Headers](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS) might be needed ) 

## usage

> bash tardigraph.sh

or if continuing an existing diagram:

> bash tardigraph.sh https://pad.somewhere.org/p/pad-name

This will create a series of files in the folder:

- **pad-name.html** (a live editable page that joins the pad view and the diagram view)
- **pad-name.txt** (the exported plain text content of the etherpad sheet)
- **pad-name.svg** (the diagram that graphviz outputs by processing the text file)
- **pad-name.pdf** (the pdf version of the diagram)

And will continue updating this files and the live view in the browser every 10 seconds, to comply with the default rate limiting of recent Etherpad installations.

## legacy

Realized in occasion of the [Energy Giveaway at the Humuspunk Library](https://criticalmedialab.ch/energy-giveaway/) exhibition, which included the ['so-and-sovereignty diagram'](https://titipi.org/diag/so-and-sovereignty.pdf). The making of the diagram made it clear we wanted to activate this mode of writing/thinking/relating collectively, allowing multiple other diagrams to be generated with a similar setup.

Directly inspired by the [Open Source Publishing's Graphviz Live](http://osp.kitchen/workshop/portrait-of-a-community/tree/master/graphviz-live-master/) setup that made clear that Graphviz can be fun with a bunch of motivated/curious people!

## license

[CC4r Collective Conditions for Re-Use](https://constantvzw.org/wefts/cc4r.en.html)   
Martino Morandi / TITiPI / 2023-2025
